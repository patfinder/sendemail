﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using NLog;
using Application = Microsoft.Office.Interop.Excel.Application;

namespace SendEmail
{
    public partial class Form1 : Form
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //    Console.WriteLine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            //    Console.WriteLine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            txtEmailSubject.Text = configuration.AppSettings.Settings["EmailSubject"].Value;
            txtEmailBody.Text = configuration.AppSettings.Settings["EmailBody"].Value;
        }

        private void ChangeEmail_Click(object sender, EventArgs e)
        {
            new ChangePassword().ShowDialog();
        }

        private void btnSendEmails_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtListFile.Text))
            {
                MessageBox.Show("Please choose email list file.");
                return;
            }

            if (!File.Exists(txtListFile.Text))
            {
                MessageBox.Show("List file not exist.");
                return;
            }

            var sendList = ReadExcelFile(txtListFile.Text);

            Cursor.Current = Cursors.WaitCursor;


            var emailSubject = txtEmailSubject.Text;
            var emailBody = txtEmailBody.Text;

            // Auto save email subject, body
            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings["EmailSubject"].Value = emailSubject;
            configuration.AppSettings.Settings["EmailBody"].Value = emailBody;
            configuration.Save(ConfigurationSaveMode.Modified);

            SendEmails(emailSubject, emailBody, sendList);
            Cursor.Current = Cursors.Default;
        }

        public List<Tuple<string, string>> ReadExcelFile(string listFile)
        {
            //Create COM Objects. Create a COM object for everything that is referenced
            Application xlApp = new Application();
            Workbook xlWorkbook = xlApp.Workbooks.Open(listFile);
            _Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Range xlRange = xlWorksheet.UsedRange;

            int rowCount = xlRange.Rows.Count;
            //int colCount = xlRange.Columns.Count;

            int emailColumn = 1, attachementColumn = 2;
            var emaiFileList = new List<Tuple<string, string>>(rowCount);

            //iterate over the rows and columns and print to the console as it appears in the file
            //excel is not zero based!!
            for (int i = 1; i <= rowCount; i++)
            {
                var email = xlRange.Cells[i, emailColumn];
                var attachement = xlRange.Cells[i, attachementColumn];

                if (email == null || email.Value2 == null || attachement == null || attachement.Value2 == null)
                {
                    // Log error
                    _logger.Error($"Line [{i}] in not correct: Email({email?.Value2}) - Attachement({attachement?.Value2})");
                    continue;
                }

                emaiFileList.Add(Tuple.Create(email.Value2, attachement.Value2));
            }

            //cleanup
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //rule of thumb for releasing com objects:
            //  never use two dots, all COM objects must be referenced and released individually
            //  ex: [somthing].[something].[something] is bad

            //release com objects to fully kill excel process from running in the background
            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);

            //close and release
            xlWorkbook.Close();
            Marshal.ReleaseComObject(xlWorkbook);

            //quit and release
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);

            return emaiFileList;
        }

        private void SendEmails(string emailSubject, string emailBody, List<Tuple<string, string>> sendList)
        {
            try
            {
                var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                var basicCredential = new NetworkCredential(
                    configuration.AppSettings.Settings["EmailUser"].Value,
                    PasswordUtils.Decrypt(configuration.AppSettings.Settings["EmailPassword"].Value));

                // Setup up the host, increase the timeout to 5 minutes
                var smtpClient = new SmtpClient
                {
                    Host = configuration.AppSettings.Settings["SmtpServer"].Value,
                    Port = int.Parse(configuration.AppSettings.Settings["SmtpPort"].Value),
                    EnableSsl = bool.Parse(configuration.AppSettings.Settings["SmtpUseSsl"].Value),
                    UseDefaultCredentials = false,
                    Credentials = basicCredential,
                    Timeout = (60*5*1000)
                };

                var fromAddress = new MailAddress(configuration.AppSettings.Settings["EmailAddress"].Value);
                var sendCount = 0;
                sendList.ForEach(sendTo =>
                {
                    var message = new MailMessage
                    {
                        From = fromAddress,
                        Subject = emailSubject,
                        IsBodyHtml = false,
                        Body = emailBody,
                        To = {sendTo.Item1}
                    };

                    message.Attachments.Add(new Attachment(sendTo.Item2));
                    smtpClient.Send(message);

                    sendCount++;
                });

                MessageBox.Show($"Send emails successfully. Total emails send: {sendCount}");
            }
            catch (Exception ex)
            {
                _logger.Error($"{nameof(SendEmails)}: Exception: {ex}");
                MessageBox.Show($"Error while sending email: {ex.Message}");
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            var browseFile = new OpenFileDialog
            {
                Filter = "Excel 2007 (*.xlsx)|*.xlsx|Excel 2003 (*.xls)|*.xls",
                CheckFileExists = true
            };

            var result = browseFile.ShowDialog();
            if(result == DialogResult.Cancel)
                return;

            txtListFile.Text = browseFile.FileName;
        }
    }
}
