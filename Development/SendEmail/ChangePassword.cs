﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SendEmail
{
    public partial class ChangePassword : Form
    {
        public ChangePassword()
        {
            InitializeComponent();

            this.KeyDown += ChangePassword_KeyDown;
        }

        private void ChangePassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.Alt && e.KeyCode == Keys.D)
            {
                // Decode password.
                try
                {
                    var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    MessageBox.Show(PasswordUtils.Decrypt(configuration.AppSettings.Settings["EmailPassword"].Value));
                }
                catch(Exception ex)
                {
                    MessageBox.Show($"Error: {ex.Message}");
                }
            }
        }

        private void ChangePassword_Load(object sender, EventArgs e)
        {
            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            txtEmail.Text = configuration.AppSettings.Settings["EmailAddress"].Value;
            //Console.WriteLine(PasswordUtils.Decrypt(configuration.AppSettings.Settings["EmailPassword"].Value));
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtEmail.Text))
            {
                MessageBox.Show("Please enter Email address.");
                return;
            }

            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                MessageBox.Show("Please enter Password & Confirm password.");
                return;
            }

            if (txtPassword.Text != txtConfirmPassword.Text)
            {
                MessageBox.Show("'Password' and 'Confirm Password' do not match.");
                return;
            }

            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings["EmailAddress"].Value = txtEmail.Text;
            configuration.AppSettings.Settings["EmailUser"].Value = txtEmail.Text;
            configuration.AppSettings.Settings["EmailPassword"].Value = PasswordUtils.Encrypt(txtPassword.Text);
            configuration.Save(ConfigurationSaveMode.Modified);

            MessageBox.Show("Email and Password saved successfully.");
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
