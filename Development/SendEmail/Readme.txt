** Cấu hình tham số chương trình
1. Mở SendEmail.exe.config
2. Sửa trường SmtpServer
3. Sửa trường SmtpUseSsl thành "false" nếu không dùng SSL

** Chạy chương trình
1. Chạy chương trình lên
2. Nếu đây là lần đầu tiên bạn chạy chương trình (hoặc nếu cần thay đổi địa chỉ người gởi), nhấn nút "Change Email"
3. Trong form này bạn có thể nhập địa chỉ email mới và password để gởi email.
4. Nhấn "Save" để lưu email mới của bạn

** Gởi email
1. Nhấn "Browse" để chọn file Excel chứa danh sách email và file đính kèm cần gởi.
2. Nhấn "Send Emails" để bắt đầu gởi. Khi chương trình gởi xong sẽ hiển thị thông báo kết quả.

** Định dạng file Excel danh sách gởi.
1. Mở EmailList.xlsx đi kèm với chương trình để xem ví dụ
2. Trong cột đầu bạn nhập Email của người nhận (1 email)
3. Trong cột thứ hai bạn để đường dẫn file cần đính kèm (1 file)

% Xin gọi Vương (0918 798 816) nếu bạn có thắc mắc về cách sử dụng chương trình
% Nếu bạn chạy chương trình và gặp lỗi, xin gởi file (Logs\SendEmail-2016xxxx.log) (nếu có) trong thư mục chứa chương trình về cho tác giả.
